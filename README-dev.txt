9 Apr 2019
* Be sure to add react-bootstrap in package.json if using react-bootstrap
* design keys:
	- build a reactive app space where you start with little 
          fish-like creatures that run toward the user's mouse.Click 
          generates more fishes
* Abide by general git branch practices - viz. if a new branch is created, a
  corresponding readme must be accompanied by the date stamp. The removal of
  this description txt is unwarrented untill branch merge.
