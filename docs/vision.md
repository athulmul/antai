# GOALS

- To unite all the technologies and techniques that can break ML systems
- To make it accessable to end users
- To always be open source

# PRODUCTS

## Antai react based app

- Provide a simple out of the box solution to add random matrix vectors to client photos, text and videos so that ML is blocked or heavily biased

## Antai Web

- Make a simple website as a service for users to upload their content and generate ML breaking content