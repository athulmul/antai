from keras_preprocessing import image

# load images
img = image.load_img("matrix.png")
noise_img = image.load_img("noisymatrix.png")

# convert images into array
img_array = image.img_to_array(img)  # shape:(720, 720, 3)
noisy_img_array = image.img_to_array(noise_img)  # shape:(720, 720, 3)

if (img_array==noisy_img_array).all():
    print('noisy img == img ')
else:
    print('added noise successfully')