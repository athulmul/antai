from keras_preprocessing import image
import numpy as np

PIXEL_RANGE = 720
COLOUR_RANGE = 255

# noise generator function
def noise_generator(pixel_range, colour_range):
    """Gets the pixel range and colour range for an image,
    pick out a random pixel element and a random colour range
    and return it
    """
    random_pixel_x = np.random.randint(pixel_range)    
    random_pixel_y = np.random.randint(pixel_range)    
    random_colour = np.random.randint(colour_range)
    return random_pixel_x,random_pixel_y, random_colour


# load image
img = image.load_img("matrix.png")

# convert image to numpy array
img_array = image.img_to_array(img)  # shape:(720, 720, 3)

# add random noise samples for around 10 pixels in each channel (RGB)
for i in range(3):
    for _ in range(10):
        x, y, noise = noise_generator(PIXEL_RANGE, COLOUR_RANGE)
        img_array[x][y][i] = noise
    
noisy_img = image.array_to_img(img_array)
noisy_img.save("noisymatrix.png")

