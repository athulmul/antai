"""
https://pythonise.com/series/learning-flask/flask-uploading-files
"""
import os
from flask import Flask, request, redirect, render_template

UPLOAD_FOLDER = '/downloads'

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

@app.route('/', methods=['GET', 'POST'])
def index():
    if request.method == 'POST':
        # check if the post request has the file part
        if request.files:
            image_file = request.files['image']
            image_file.save(os.getcwd() + app.config["UPLOAD_FOLDER"] +"/"+ image_file.filename )
            print("Image saved")
            print(request.url)
            return redirect(request.url)
    return render_template("index.html")


if __name__ == "__main__":
    app.run(debug=True)