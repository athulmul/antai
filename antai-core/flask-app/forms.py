from flask_wtf import FlaskForm
from wtforms import FileField

class ImageUploadForm(FlaskForm):
    image_file = FileField('Image File')