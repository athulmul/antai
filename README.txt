CONTENT DESCRIPTION

antai-blank-nav/:
	* test antai app in react native demonstrating stack-navigation
	* has its own .git

code/:
	* antai test app in react native with navigation template
	* has its own .git
docs/:
	* antai docs. includes vision, logo, legal and economics
ml/:
	* the machine learning repository of antai

react-antai-web/:
	* react version of antai-web
	* has its own .git
site/:
	* root repo of antai-web
	* has its own .git

data/: 
	* place to store all the data needed for ml and other training
	* is added to .gitignore to save space

flutter/:
	* flutter implimentaion of the antai app

papers/:
	* relevant scientific papers for antai	

more at https://antai.nl/