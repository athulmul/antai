28jul19-----------------------------------------------------------------
* impliment the adveserial attack paper

1aug19-------------------------------------------------------------------
* adveserial-tf:
	- implimentation of adveserial example provided by tensorflow site
	- not implimented as of 1 aug 19
	- preturbations not implimented

* adverserial-tf-cuda:
	- adverserial example from tensorflow site with CUDA
	- (29 Jul 19) This error is a non compatibility issue with conda 
            installs of tensorflow-gpu. The cuDNN is not being supported

* fashion-mnist-tf:
	- basic fashion mnist implimentation from tensorflow site

* mnist-fgsm:
	- fgsm example from cleverhans tutorial library
	- not implimented: pdm_post_mortem error
