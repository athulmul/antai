import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;
//======================================MAIN===================================================================================
void main(){
  runApp(MaterialApp(
    title: 'Antai',
    home: HomeScreen(),
  ));
}

//=========================================HOMESCREEN===========================================================================
class HomeScreen extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Home Screen'),
      ),
      body: ListView(
        padding: EdgeInsets.symmetric(horizontal: 3.0
        ),
        children: <Widget>[
          MaterialButton(
//            https://medium.com/flutterpub/anatomy-of-material-buttons-in-flutter-first-part-40eb790979a6
          // TODO: implient percentages to the rightside of the Flatbutton Widget - 4JUL2019
            child:Align(child:Text('Google'),alignment: Alignment.centerLeft),
            onPressed: (){
              Navigator.push(context, MaterialPageRoute(builder: (context)=>OverviewScreen()));
              },
            color: Colors.blue,
            textColor: Colors.white,
            splashColor: Colors.lightBlue,
            minWidth: 15.0,
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
            key: ValueKey('Google')
          ),
        ],
      ),
    );
  }
}

//==========================PIECHART============================================================================================
//https://medium.com/flutter/beautiful-animated-charts-for-flutter-164940780b8c
//https://google.github.io/charts/flutter/example/pie_charts/outside_label

//https://fluttersensei.com/posts/the-toobox-charts-for-flutter/

class PieChartWidget extends StatelessWidget{
  final List<charts.Series> seriesList;
  final bool animate;

  PieChartWidget(this.seriesList,{this.animate=true});

  @override
  Widget build(BuildContext context) {
    return charts.PieChart(
      seriesList,
      animate: animate,
    );
  }
}

class QuarterlySales {
  final String quarter;
  final double sales;

  QuarterlySales(this.sales,this.quarter);
}

//============================OVERVIEWSCREEN====================================================================================
class OverviewScreen extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Overview Screen')),
      body: Center(child: Placeholder()),
    );
  }
}

